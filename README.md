0# README #


### What is this repository for? ###

* The plugin in is intended to run all matrix elements for the mixed QCD-EW NLO corrections for the process g g > H g as a standalone with MG5.

### How do I get set up? ###

* install MadGraph5_aMC@NLO VERSION 2.7.3    
    `> wget https://launchpad.net/mg5amcnlo/2.0/2.7.x/+download/MG5_aMC_v2.7.3.tar.gz`     
* clone the repo in madgraphs /PLUGIN/ directory    
* rename the plugin to `higgsew`    
    `> mv mg5_higgs_ew_plugin higgsew`     
* To get the grids install either `git lfs` or download the static version with     
    
    `> wget http://madgraph.physics.illinois.edu/Downloads/PLUGIN/higgsew.tar.gz`    
    
* extract the grids in PATHTOPLUGIN/higgsew/ComputationFormFacGGHGEW/mathematicaRoutines    
    
    `> tar -xf tar -xf NP_EW1_24_B.tar.gz`     
    `> tar -xf Planar_EW1_24_B.tar.gz`   
    

### Minimal usage: ###
* use    
    `> python2 ./mg5_aMC --mode=higgsew`      
* type:
    
    `> import model PATHTO/MG5_aMC_v2_6_7/PLUGIN/higgsew/UFO_model_gggH-xsection `    
    `> generate g g > H g GGGHEFT^2==2 QCD^2==6 @1`         
    `> output standalone_ggHg EXPORTDIRNAME`        
    `> launch -f`  
    

### Coupling orders and process description: ###
* GGGHEW: effective coupling for real emission EW amplitude (W and Z)    
* GGHEW:  effective coupling for virtual EW amplitudes (W and Z)    
* ZZ and WW (lower hierarchy): Z (resp. W) exchange only   

* GGGHEFT: effective coupling for real emission HEFT amplitude    
* GGHEFT:  effective coupling for virtual HEFT amplitudes    

* The definiton of all available processes is shown on start-up. Sample runcards are provided in: PATHTOPLUGIN/higgsew/runcard_templates    


### Offline parallelization for real emission matrix elements (basic): ###
* The offline parallelization is based on filling grids. A grid has the following form:   
  First entry: number of points in the grid;        
  Following entries: phase-space points and coupling information in the form: components of all external momenta (16 values), strong coupling constant gs, renormalization scale muR    
* A sample grid is: PATHTOPLUGIN/higgsew/GridFiller/grid_template_to_process_15k.dat

* Minimal syntax example:    
* Create the process: (see PATHTOPLUGIN/higgsew/runcard_templates/ggHg_real_HEFT.mg5)    
    `>  PATHTO/bin/mg5_aMC --mode=higgsew ggHg_real_HEFT.mg5`    
* Fill the grids (see: PATHTOPLUGIN/higgsew/GridFiller/fill_grids.py):    
    `> python3.7 ./fill_grids.py -c 36 -i "PATHTOGRID.dat" -o "PATHTOOUTPUTGRID" -p "PATHTO/HIGGSEW_LI_at_NLO_REAL_HEFT"`    
* For the options see:    
    `> python3.7 ./fill_grids.py -h`    
* To abort the computation gracefully run "touch stop" in the same directory as the fill_grids.py.
* Adjust the linking in the `Makefile`. If the process was generated with @4, the linking in the `Makefile` has to be changed to `-lPROC_4` instead of `-lPROC_2`.    


### Offline parallelization for real emission matrix elements in "fifo"-mode: ###
* For the parallelization of the real emission electroweak matrix elements one needs the dynamic series expansion of the Master Integrals (MIs). This series expansion is performed in Mathematica 12.03. In particular, when this expansion is performed on many cores in parallel the computation with Mathematica can run into a lock state. To avoid this, one can run in "fifo"-mode. In "fifo"-mode, the computation is performed without starting a new kernel for every PS-point. To use "fifo"-mode proceed as followed:   
* Create a fifo-file:    
    `>  cd ComputationFormFacGGHGEW/mathematicaRoutines`    
    `>  mkfifo mathematica_input.fifo`    
    
* Create the matrix-element standalone with the fifo option (see: higgsew/runcard_templates/ggHg_real_EW_fifo.mg5)    
    `> python2 ./mg5_amc --mode higgsew`     
    `> import model PATHTO/MG5_aMC_v2_6_7/PLUGIN/higgsew/UFO_model_gggH-xsection`    
    `> set_IO_mode fifo`     
    `> generate g g > H g GGGHEFT^2==1 GGGHEW^2==1 QCD^2==6 @2`    
    `> !rm -rf HIGGSEW_LI_at_NLO_REAL_fifo`    
    `> output standalone_ggHg HIGGSEW_LI_at_NLO_REAL_fifo`    
    
* Run the grid-filler with "fifo"-mode (start more listeners then kernels):    
    `> python3.7 ./fill_grids.py -c 30 -f 45 -i "PATHOTOGRID" -o "PATHTOOUTPUTGRID" -p "PATHTO/HIGGSEW_LI_at_NLO_REAL_fifo/"`    
* If a point is deep in the IR, it may take long: adjust the parameters:    
        `MAX_ATTEMPTS = 3`    
        `BASE_TIME = 7000.0 # in seconds`    
        `TIME_INCREMENT = 300.0 # in seconds`    
in the fill_grids.py to higher values. Points which are not computed in time will be written as NaN in the output file. Successive runs will only compute the NaN values. One often needs more then one iteration to get the grid filled without any NaN's. If after an aborted run there are still Mathematica-zombie-jobs running, use:    
    `> ps -ef | grep 'mathem' | grep -v grep | awk '{print $2}' | xargs -r kill -9`     


### Adding processes? ###

The plugin can in principle be extended for different processes, if the UFO is extended correctly. Then, one can append the process to the process_information.json, given that all relevant routines exist. However, we strongly encourage interested users to contact us for further details before starting.